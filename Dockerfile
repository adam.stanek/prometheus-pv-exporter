FROM golang:1.14-stretch AS build
ADD src /app/src
ADD go.mod /app/
ADD go.sum /app/
WORKDIR /app
ARG CI_COMMIT_SHORT_SHA
RUN go build -ldflags "-X main.GitCommit=$CI_COMMIT_SHORT_SHA" -o ./bin/pv-exporter ./src/*.go

FROM debian:stretch
COPY --from=build /app/bin/pv-exporter /app/bin/pv-exporter
WORKDIR /app
CMD ["/app/bin/pv-exporter"]