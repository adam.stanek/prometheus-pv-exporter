package main

import (
	"fmt"
	"path/filepath"

	"github.com/rs/zerolog/log"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

func getConfig() *rest.Config {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Warn().Msg("Unable to use in-cluster config, falling back to kubeconfig")

		if home := homedir.HomeDir(); home != "" {
			kubeconfigPath := filepath.Join(home, ".kube", "config")
			config, err = clientcmd.BuildConfigFromFlags("", kubeconfigPath)
			if err != nil {
				log.Fatal().Str("kubeconfig", kubeconfigPath).Str("err", err.Error()).Msg("Unable to create config")
			} else {
				log.Info().Str("path", kubeconfigPath).Msg("Using kubeconfig")
			}
		} else {
			log.Fatal().Msg("Unable to determine home directory")
		}
	} else {
		log.Info().Msg("Using in-cluster config")
	}

	return config
}

// See: https://gianarb.it/blog/kubernetes-shared-informer
func handleK8Volumes() {
	clientset, err := kubernetes.NewForConfig(getConfig())
	if err != nil {
		log.Fatal().Str("err", err.Error()).Msg("Unable to create clientset")
	}

	factory := informers.NewSharedInformerFactory(clientset, 0)
	informer := factory.Core().V1().PersistentVolumes().Informer()

	stopper := make(chan struct{})
	defer close(stopper)
	defer runtime.HandleCrash()

	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			pv := obj.(*corev1.PersistentVolume)
			go stateManager.AddVolume(pv)
		},
		DeleteFunc: func(obj interface{}) {
			pv := obj.(*corev1.PersistentVolume)
			go stateManager.RemoveVolume(pv)
		},
		UpdateFunc: func(oldObj interface{}, newObj interface{}) {
			oldPv := oldObj.(*corev1.PersistentVolume)
			newPv := newObj.(*corev1.PersistentVolume)
			go stateManager.ReplaceVolume(oldPv, newPv)
		},
	})

	go informer.Run(stopper)

	if !cache.WaitForCacheSync(stopper, informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	<-stopper
}
