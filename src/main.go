package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// GitCommit - Injected on CI (from CI_COMMIT_SHORT_SHA)
var GitCommit string

var pvStats = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name:      "bytes",
	Namespace: "host_pv",
	Help:      "Number of bytes inside a volume",
}, []string{"pv", "pvc", "hostPath"})

var stateManager = NewStateManager()

func serve() {
	port := 8080

	http.Handle("/metrics", promhttp.Handler())

	log.Info().Int("port", port).Msg("Starting HTTP server")
	err := http.ListenAndServe(fmt.Sprintf(":%v", port), nil)
	if err != nil {
		log.Fatal().Int("port", port).Err(err).Msg("Unable to start HTTP server")
	}
}

func getRootPath() string {
	rootPath := os.Getenv("ROOT_PATH")
	if rootPath != "" {
		return rootPath
	}

	return "./data"
}

func main() {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	consoleWriter := zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC822}
	log.Logger = log.Output(consoleWriter)

	initMsg := log.Info()
	if GitCommit != "" {
		initMsg.Str("gitversion", GitCommit)
	}

	initMsg.Msg("Application started")

	err := prometheus.Register(pvStats)
	if err != nil {
		log.Fatal().Msg("Unable to register prometheus gauge")
	}

	go collect(getRootPath())
	go handleK8Volumes()

	serve()
}
