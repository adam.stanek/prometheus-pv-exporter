package main

import (
	"fmt"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
	corev1 "k8s.io/api/core/v1"
)

// Volume - Holds state info about single PV
type Volume struct {
	Name     string
	HostPath string

	ClaimNamespace string
	ClaimName      string

	Skipped bool
}

// Report - Reports measured value
func (v *Volume) Report(value *float64) {
	if value != nil {
		pvStats.With(v.PrometheusLabels()).Set(*value)
	} else {
		v.UnregisterFromPrometheus()
	}
}

// PrometheusLabels - Returns map of prometheus labels
func (v *Volume) PrometheusLabels() prometheus.Labels {
	return prometheus.Labels{
		"pv":       v.Name,
		"pvc":      fmt.Sprintf("%v/%v", v.ClaimNamespace, v.ClaimName),
		"hostPath": v.HostPath,
	}
}

// UnregisterFromPrometheus - Removes volume from prometheus metrics
func (v *Volume) UnregisterFromPrometheus() {
	pvStats.Delete(v.PrometheusLabels())
}

// StateManager - State manager context
type StateManager struct {
	Volumes map[string]*Volume
	Mutex   sync.RWMutex
}

// NewStateManager - Creates new state manager context
func NewStateManager() *StateManager {
	return &StateManager{
		Volumes: make(map[string]*Volume),
	}
}

// AddVolume - Registers volume in thread-safe manner
func (manager *StateManager) AddVolume(pv *corev1.PersistentVolume) {
	manager.Mutex.Lock()
	if _, ok := manager.Volumes[pv.Name]; ok {
		log.Fatal().Str("name", pv.Name).Msg("Invalid state, volume has been already registered")
	} else {
		if pv.Spec.HostPath != nil {
			if pv.Spec.ClaimRef != nil {
				log.Debug().Str("name", pv.Name).Msg("Registering volume")
				manager.Volumes[pv.Name] = &Volume{
					Name:           pv.Name,
					HostPath:       pv.Spec.HostPath.Path,
					ClaimNamespace: pv.Spec.ClaimRef.Namespace,
					ClaimName:      pv.Spec.ClaimRef.Name,
					Skipped:        false,
				}
			} else {
				log.Debug().Str("name", pv.Name).Msg("Ignoring unbound volume")
			}
		} else {
			log.Debug().Str("name", pv.Name).Msg("Ignoring non HostPath volume")
		}
	}
	manager.Mutex.Unlock()
}

// RemoveVolume - Unregisters volume in thread-safe manner
func (manager *StateManager) RemoveVolume(pv *corev1.PersistentVolume) {
	log.Debug().Str("name", pv.Name).Msg("Unregistering volume")
	manager.Mutex.Lock()
	if volume, ok := manager.Volumes[pv.Name]; ok {
		delete(manager.Volumes, pv.Name)
		volume.UnregisterFromPrometheus()
	}

	manager.Mutex.Unlock()
}

// ReplaceVolume - Replaces existing volume info with new one
func (manager *StateManager) ReplaceVolume(oldPv *corev1.PersistentVolume, newPv *corev1.PersistentVolume) {
	manager.RemoveVolume(oldPv)
	manager.AddVolume(newPv)
}

// GetVolumes - Returns list of registered volumes in thread-safe manner
func (manager *StateManager) GetVolumes() []*Volume {
	manager.Mutex.RLock()

	i := 0
	volumes := make([]*Volume, len(manager.Volumes))

	for _, volumeState := range manager.Volumes {
		volumes[i] = volumeState
		i++
	}

	manager.Mutex.RUnlock()

	return volumes
}
