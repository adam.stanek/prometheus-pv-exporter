package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"
)

var dfRX = regexp.MustCompile(`^[0-9]+`)

func calculateDirSize(path string) *float64 {
	out, err := exec.Command("du", "-sb", path).Output()
	if err != nil {
		log.Error().Err(err).Msg("DF Execution failed")
		return nil
	}

	bytesStr := dfRX.FindString(string(out))
	if bytesStr == "" {
		return nil
	}

	bytes, parseErr := strconv.ParseFloat(bytesStr, 64)
	if parseErr != nil {
		log.Error().Err(parseErr).Msg("Unable to parse DF output")
		return nil
	}

	return &bytes
}

func collect(basePath string) {
	time.Sleep(2 * time.Second)
	log.Info().Str("root", basePath).Msg("Starting collection")

	for {
		for _, volume := range stateManager.GetVolumes() {
			if !volume.Skipped {
				log.Trace().Str("pv", volume.Name).Msg("Processing volume")

				path := filepath.Join(basePath, volume.HostPath)
				if _, err := os.Stat(path); err != nil {
					if os.IsNotExist(err) {
						log.Warn().Str("path", path).Msg("File path does not exist, marking volume to skip")
					} else {
						log.Warn().Str("path", path).Err(err).Msg("Unable to access file path, marking volume to skip")
					}

					volume.Skipped = true
					volume.UnregisterFromPrometheus()
				} else {
					log.Debug().Str("pv", volume.Name).Msg("Calculating allocated size")
					volume.Report(calculateDirSize(path))
				}
			}
		}

		time.Sleep(2 * time.Minute)
	}
}
