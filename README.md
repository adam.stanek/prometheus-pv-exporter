# Prometheus exporter for HostPath volumes on Kubernetes

Exports byte allocation for all HostPath volumes found in the Kubernetes cluster

- Starts HTTP server on 8080, metrics available at `/metrics`
- Expects volumes to be mounted in virtual root directory (configurable by `ROOT_PATH`)
- Currently only supports single node clusters
- Allocation is calculated every 2 minutes, might want to spread that in time in the future to avoid continuous IO load
- Using [du](https://linuxcommand.org/lc3_man_pages/du1.html) for calculation
