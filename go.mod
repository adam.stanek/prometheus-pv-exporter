module gitlab.com/adam.stanek/prometheus-pv-exporter

go 1.15

require (
	github.com/google/gofuzz v1.2.0 // indirect
	github.com/google/martian/v3 v3.0.0
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/prometheus/client_golang v1.9.0 // indirect
	github.com/rs/zerolog v1.20.0
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620 // indirect
	golang.org/x/net v0.0.0-20201216054612-986b41b23924 // indirect
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	k8s.io/api v0.19.0
	k8s.io/apimachinery v0.19.0
	k8s.io/client-go v0.19.0
	k8s.io/utils v0.0.0-20201110183641-67b214c5f920 // indirect
)
